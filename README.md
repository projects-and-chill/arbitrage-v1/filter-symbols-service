# Before starting:
- For exchanges that need authentication, you should provider **API credentials** in database `credentials`
- If you don't have API KEY for an exchange, you should exclude them before launching arbitrage processing.

```typescript
//----Payload type----:
interface ICredential {
  market: string,
  apiKey?: string,
  secret?: string,
  password?: string
}

//----Payload exemple----:
[
  {
    market: "OKEX",
    apiKey: "MY_API_KEY",
    secret: "MY_API_KEY_SECRET",
    password: "MY_API_KEY_PASSWORD"
  }
]
```
