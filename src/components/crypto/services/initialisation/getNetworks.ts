import {Symbol} from "../../../../models/interfaces/symbol";
import {Network} from "../../../../models/interfaces/network";
import {CurrencyEnriched} from "../../../../models/interfaces/currencyEnriched";
import {getSymbolInformation} from "../../../cctx/getSymbolInformation";


const networkAliases = {
  ETH: "ERC20",
  TRX: "TRC20",
  BSC: "BEP20",
  BNB: "BEP2",
  HT: "HECO",
  OMNI: "OMNI"
}

//Prend les networks a partir des symboles finaux
export async function getNetworks(symbols: Symbol[]) {

  console.log('network part-1')
  const marketSet: Set<string> = new Set()
  const marketsByAsset: Map<string, Set<string>> = new Map()
  symbols.forEach(symbol => {
    marketSet.add(symbol.market)
    const baseSet: Set<string> | undefined = marketsByAsset.get(symbol.base)
    if (!baseSet)
      marketsByAsset.set(symbol.base, new Set([symbol.market]))
    else
      baseSet.add(symbol.market)
    const quoteSet: Set<string> | undefined = marketsByAsset.get(symbol.quote)
    if (!quoteSet)
      marketsByAsset.set(symbol.quote, new Set([symbol.market]))
    else
      quoteSet.add(symbol.market)
  })

  console.log('network part-2')
  let marketsCurrencies: Record<string, Record<string, CurrencyEnriched>> = {}
  for (const market of marketSet) {
    const currencies: Record<string, CurrencyEnriched> = await getSymbolInformation(market)
    marketsCurrencies[market] = currencies || {}
  }
  console.log('network part-3')
  const networks: Network[] = []
  let countNoCurrencies = 0
  for (const [asset, marketSet] of marketsByAsset) {
    marketSet.forEach(market => {
      const currency: CurrencyEnriched | undefined = marketsCurrencies[market][asset]
      if (!currency || !currency.networks) {
        //Count object that didn't contains network or currencies informations
        countNoCurrencies++
        return
      }

      const assetNetworks = Object.values(currency.networks)
      networks.push(...assetNetworks.map<Network>(networkData => {

        if ('depositEnable' in networkData)
          networkData.deposit = networkData.depositEnable
        if ('withdrawEnable' in networkData)
          networkData.withdraw = networkData.withdrawEnable

        const network = networkData.network.toUpperCase().replace(/[-_ ]/,"")
        const networkAlias : string = networkAliases[network]
        return {
          name: `${networkAlias ? networkAlias : network}_${market}_${asset}`,
          network: networkAlias ? networkAlias : network,
          asset,
          market,
          withdraw: networkData.withdraw === undefined ? null : networkData.withdraw,
          deposit: networkData.deposit === undefined ? null : networkData.deposit,
          active: typeof networkData.active === 'boolean'
            ? networkData.active
            : (typeof networkData.withdraw === 'boolean' && typeof networkData.deposit === 'boolean')
              ? networkData.withdraw || networkData.deposit
              : null
        }
      }))
    })
  }
  console.log('Asset without network informations: ', countNoCurrencies)
  console.log('network part-4', "network length", networks.length)
  return networks
}
