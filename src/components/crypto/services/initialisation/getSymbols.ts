import axios from 'axios'
import {Market} from "../../../../models/interfaces/market";
import {Symbol, SymbolFor} from "../../../../models/interfaces/symbol";

import {Asset} from "../../../../models/interfaces/asset";
import {COINAPI_URL} from "../../../../config/globals";
import {symbol_type} from "../../config_init";
import {END_GRAPH, PAS_GRAPH, START_GRAPH} from "../../../bests/config_bests";


interface resp_symbols {
  symbol_id: string,
  symbol_type: 'SPOT' | 'FUTURES'
  asset_id_base: string,
  asset_id_quote: string,
  volume_1day_usd: number,
  exchange_id : string
}

async function strMarketsNames (markets : Market[]) : Promise<string> {
  let str : string = ''
  for  (let market of markets)
    str += `${market.name},`
  return str
}

async function  strAssetsNames(assets : Asset[]) : Promise<string> {
  let str : string = ''
  for (let asset of assets) {
    str += `${asset.name},`
  }
  return str
}

/*To fixed
market 125 = 1000
asset 1515 = 7320
total = 8300
ERROR
----------------
markets 125 = 1000
asset 1300 = 6200
total = 7200
SUCESS --> Donc Le maximum de caracteres dans l'url doit être de 7200 caracteres
*/



//Recupere les symbols sur coinapi en fonction des markets et des paires demandées
async function getSymbols (markets : Market[], assets : Asset[]) :  Promise<Symbol[]> {
  let url = `${COINAPI_URL}/v1/symbols`
  console.log('--1--')
  // let [strAssets, strMarkets] = await Promise.all([
  //   strAssetsNames(assets)
  //   // strMarketsNames(markets),    // solution de contournement 20/06/2022
  // ])
  console.log('--2--')
  let {data : symbols } : { data : resp_symbols[] } =  await axios.get(url, {params : {
      // filter_asset_id : strAssets,
      // filter_exchange_id : strMarkets     // solution de contournement 20/06/2022
  }})

  const side : SymbolFor['buy' | 'sell'] = {
    bestMarketFreq : 0,
    okFreq : 0,
    notDataFreq : 0,
    notEnoughVolFreq : 0,
    prixMoyen_quote :null
  }

  console.log('--3--')
  let isfor = {}
  for (let i = START_GRAPH; i <= END_GRAPH; i += PAS_GRAPH){
    isfor[i] = {
      buy : side,
      sell : side,
    }
  }

  console.log('--4--')
  symbols = symbols.filter(symbol=> /*symbol.volume_1day_usd >= symbol_volume_usd1day &&*/ symbol.symbol_type === symbol_type)
  console.log('--5--')
  console.log('filtrage 1','symbs lengh', symbols.length)
  symbols = symbols.filter(symbol => symbol.symbol_id.split('_').length === 4 ) //Filtre les symboles bizzares qui ne sont pas composés de 4 morceaux
  console.log('filtrage 2','symbs lengh', symbols.length)
  symbols = symbols.filter(symbol => markets.some(market => market.name === symbol.exchange_id) ) // solution de contournement 20/06/2022 filtrer les symbols ayant des markets absent de la liste d'assets
  console.log('filtrage 3','symbs lengh', symbols.length)
  symbols = symbols.filter(symbol => assets.some(asset => asset.name === symbol.asset_id_base || asset.name === symbol.asset_id_quote) ) // solution de contournement 20/06/2022 filtrer les symbols ayant des assets absent de la liste d'assets
  console.log('filtrage 4','symbs lengh', symbols.length)
  const filteredSymbols :  Symbol[]= symbols
    .filter(symbol=> (
      symbols.some(symb => symb.exchange_id !== symbol.exchange_id && symbol.asset_id_quote === symb.asset_id_quote) //Filtrer les symbs qui n'ont pas : 1 market en commun, 1 quote en commun
    ))
    .map(symb => ({
      name: symb.exchange_id + '_'+ symb.asset_id_base +'_'+ symb.asset_id_quote,
      market: symb.exchange_id,
      pair : symb.asset_id_base + '_'+ symb.asset_id_quote,
      symbolCoinapi: symb.symbol_id,
      base : symb.asset_id_base,
      quote : symb.asset_id_quote,
      isfor,
      exclusion: {
        isExclude: false,
        reasons: [],
        severity: 0,
        excludeBy: null,
        note: null
      },
      date : new Date()
    }))


  console.log("total symbols", filteredSymbols.length)
  return filteredSymbols
}


export default getSymbols
