import ccxt from "ccxt"
import exchangeMappings from '../../exchangeMapping.json'
import {CurrencyEnriched} from "../../models/interfaces/currencyEnriched";
import {ICredential, modelCredential} from "../../models/mongoose/modelCredential";


export const getSymbolInformation = async (exchangeCoinapi: string) :  Promise< Record<string, CurrencyEnriched> | null >  => {
  const exchangeData : typeof exchangeMappings[number] | undefined = exchangeMappings.find(exchangeMapping => exchangeMapping.coinapi === exchangeCoinapi )
  if (!exchangeData) return null
  const exchangeCred : Partial<ICredential> | undefined = await modelCredential.findOne({ market: exchangeCoinapi }, "-market") || {}
  const exchange = new ccxt[exchangeData.cctx](exchangeCred)
  await exchange.loadMarkets()
  return exchange.currencies
}

