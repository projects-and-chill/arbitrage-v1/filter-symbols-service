import axios from 'axios'
import modelSymbol from "../../models/mongoose/model.symbol";
import {Asset} from "../../models/interfaces/asset";
import modelAsset from "../../models/mongoose/model.asset";
import {Market} from "../../models/interfaces/market";
import modelMarket from "../../models/mongoose/model.market";
import debuger from "debug";
import {COINAPI_URL} from "../../config/globals";
import {PriceV2} from "../../models/interfaces/price";
import ErrorsGenerator from "../ErrorsGenerator";
import {StatusCodes} from "http-status-codes";
import modelOrderbook from "../../models/mongoose/model.orderbook";
import {makePricesV2} from "./makePricesV2";
import {buildCombinations, ICombination} from "./build-combinations";
import {modelCombination} from "../../models/mongoose/model.combination";
import {getComparationConfig} from "./getComparationConfig";

interface Orderbook {
  symbol_id: string
  asks: Array<{
    price: number
    size: number
  }>
  bids: Array<{
    price: number
    size: number
  }>
}


interface axiosResponse {
  data? : Orderbook[],
  isAxiosError : boolean,
  response? : {
    status : number,
    statusText : string,
    data : any
  },
}

const aggregateSymbols = [
  {$match: {"exclusion.isExclude" : false}},
  {$lookup: {
      from: "pairs",
      localField: "pair",
      foreignField: "name",
      as: "pair"
    }
  },
  {$lookup: {
      from: "markets",
      localField: "market",
      foreignField: "name",
      as: "market"
    }
  },
  {$unwind: "$pair"},
  {$unwind: "$market"},
  {$match: {"market.exclusion.isExclude" : false, "pair.exclusion.isExclude" : false}},
  {$group : {
      _id : "$pair.name",
      symbs : { $push: "$symbolCoinapi" }
    }},
]

const debug = debuger("api:index-calcul")


//Execute chaque parties du programme
export async function bestV2 (skipOrderbooks: boolean) : Promise<void>{
  console.log("2- Lancement du programme calcul")

  const [symbsGroups,assets, markets] : [ Array<{_id : string,symbs:string[] }>, Asset[],Market[] ] = await Promise.all([
    modelSymbol.aggregate(aggregateSymbols),
    modelAsset.find({}).lean(),
    modelMarket.find({}).lean(),
  ])
  console.log("3- Recup symb,market, asset OK")
  if(!symbsGroups.length){
    throw new ErrorsGenerator(
      "Préconditons Requisent",
      "Il faut initialiser l'app avant de pouvoir effectuer un calcul",
      StatusCodes.PRECONDITION_REQUIRED,
    )
  }


  let symbols : string[] = []
  symbsGroups.forEach(group => symbols.push(...group.symbs))
  console.log("3 bis - Creation des groupes")

  let requestGroup = createGroupsRequest(symbsGroups)



  let raw_orderbooks
  if (skipOrderbooks) {
    console.log("4- Lancement recup db orderbooks")
    raw_orderbooks = await modelOrderbook.find({}).lean()
  }
  else
  {
    console.log("4- Lancement recup long orderbooks")
    raw_orderbooks = await getOrderbooks(requestGroup)
  }


  console.log("5- Filtrage orderbook")
  //Certains symboles ne seront pas renvoyée par l'API et d'autre seront en trop, on filtre ceux en trop et on signal ceux manquants
  const orderbooks : Orderbook[] = raw_orderbooks.filter(raw_orderbook => symbols.includes(raw_orderbook.symbol_id) )

  console.log("6- Lancement 1er gros calcul --> Fabrication des prix")
  const {usdToSpend, minimumEfficiency} = await getComparationConfig()
  const prices : PriceV2[] = await makePricesV2(orderbooks, assets, markets, usdToSpend)
  const combinations : ICombination [] = await buildCombinations(prices, minimumEfficiency)



  console.log("step 6 : Database filling")
  await modelCombination.insertMany(combinations.map(combination => ({
    combinationGroup: Date.now(),
    usdToSpend,
    ...combination
  })))

  //----------------DISPLAY RESULT IN CONSOLE ------------------------
  console.dir(combinations.map(combination => combination.efficiency), {showHidden: false, depth: null, colors: true})

  const combinationMap : Map<string, ICombination> = new Map(combinations.map(combination => [combination.pair, combination]))
  console.dir(
    [...combinationMap.values()]
      // .filter(combination => combination.efficiency < 10)
      .slice(0,20).map(result=> ({
      id: result.name,
      efficiency: result.efficiency,
      prices: { buy: result.buy.price, sell: result.sell.price }
    }))
    ,{showHidden: false, depth: null, colors: true})
  //-------------------------------------------------------------------
}

// On crée des groupes de requêtes par "pairs" , ainsi chaque groupe de symboles d'une même pair sera récupérer au même moment !
// Cela permet de préserver la cohérence malgré le temps d'attente entre chaque requêtes .
function createGroupsRequest(symbolsGroups : Array<{_id : string,symbs:string[] }>) :  Array<string[]> {
  let requestGroup : Array<string[]> = []
  let tab : Array<string> = []
  symbolsGroups.forEach((symbolGroup) => {
    let nextGroupLenght : number = symbolGroup.symbs.length
    if(tab.length + nextGroupLenght <= 100){
      tab.push(...symbolGroup.symbs)
    }else{
      requestGroup.push([...tab])
      tab = [...symbolGroup.symbs]
    }
  })
  return requestGroup
}


//On récupère l'orderbook de chaque symbole de manière synchrone
async function getOrderbooks (requestGroup : Array<string[]> ) : Promise<Orderbook[]>{
  /*DEBUGAGE*/await modelOrderbook.deleteMany({})/*DEBUGAGE*/
  console.log('total request steps (total group of params) : ',requestGroup.length)
  const x = 100/ requestGroup.length
  let result = 0
  let url = `${COINAPI_URL}/v1/orderbooks/current`
  const orderbooks : Orderbook[] = []
  for(const group of requestGroup) {
    debug("%s", "chargement: ", result.toFixed(0) , " %")
    result += x
    console.log('params lenght (group lenght) : ',group.length)
    let axiosResp : axiosResponse = await axios.get(url, {params: {filter_symbol_id: group.toString()}})
    if(axiosResp.isAxiosError)
      throw  {
        status : axiosResp.response.status,
        statusText  : axiosResp.response.statusText,
        data : axiosResp.response.data
      }
    /*DEBUGAGE*/await modelOrderbook.insertMany(axiosResp.data)/*DEBUGAGE*/
    orderbooks.push(...axiosResp.data)
  }
  return orderbooks
}






