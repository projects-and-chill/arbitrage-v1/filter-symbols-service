import {Symbol} from "../../models/interfaces/symbol";
import {Network} from "../../models/interfaces/network";
import {modelNetwork} from "../../models/mongoose/model.network";
import modelSymbol from "../../models/mongoose/model.symbol";
import {PriceV2} from "../../models/interfaces/price";


type LightSymbol = Omit<Symbol, "isfor" | "date" | "exclusion" >

export interface CombinationSide {
  symbol: string
  market: string
  networksQuote: Network[]
  price: number
}

export interface ICombination {
  name: string,
  pair: string,
  base: string,
  quote: string,
  efficiency: number
  volume: number
  buy: CombinationSide,
  sell: CombinationSide,
}

interface EnrichedPriceV2 extends PriceV2 {
  networkBase: Network[]
}



/**
 * -------NETWORKS FILTRAGE -----------
 * Attention il y a de fortes chances que le filtrage par networksIDs exclus des combinaisons valables !
 *
 * En effet, certains networkIDs peuvent avoir une nomenclature différentes selon les markets. Par exemple: "ERC20" vs "ETH".
 * Dans ces situations, l'algorithme manquera de créer ces combinaisons dont la nomemclature des networkIDs est différente.
 *
 * --> Pour régulariser celà il faudrai normaliser la nomemclature de chaque network avant de les enregistrer en DB.
 */
export const buildCombinations = async (prices: PriceV2[], minimumSpreadPercentage: number) => {
  console.log('calcul intermediaire', 'prices to check', prices.length)
  // let url = `${COINAPI_URL}/v1/quotes/current`
  // let {data: quotations}: { data: Quotation[] } = await axios.get(url)
  const networks: Network[] = await modelNetwork.find({}, '-_id').lean()
  const combinations: ICombination [] = []
  const lightedSymbols : LightSymbol[] = (await getSymbols()).filter(symbol => prices.some(price=> price.info.symbolCoinapi === symbol.symbolCoinapi))
  const pairSet: Set<string> = new Set(lightedSymbols.map(symbol => symbol.pair))

  for (const pair of pairSet) {
    const symbolList = lightedSymbols.filter(symbol => symbol.pair === pair)
    let intersectionQuotations: EnrichedPriceV2[] = [];
    for (const lightSymbol of symbolList) {
      const price: PriceV2|undefined = prices.find(price => price.info.symbolCoinapi === lightSymbol.symbolCoinapi)
      if (!price) continue
      const {market, base} = lightSymbol
      const enreichedPrice: EnrichedPriceV2 = {
        ...price,
        networkBase: networks.filter(network => network.market === market && network.asset === base),
      }
      intersectionQuotations.push(enreichedPrice)

    }
    combinations.push(...getCombinaisons(intersectionQuotations, minimumSpreadPercentage))
    pairSet.delete(pair)
  }

  return combinations.sort((a,b)=>  b.efficiency - a.efficiency)
}


function getCombinaisons(EnrichedPricesChunk: EnrichedPriceV2[], minimumSpreadPercentage: number): ICombination[] {
  let chunkLenth : number = EnrichedPricesChunk.length
  const combinations : ICombination[] =  []
  for (let i = 0; i < chunkLenth; i++) {
    const currentQuotation: EnrichedPriceV2 = EnrichedPricesChunk.pop()
    combinations.push(...buyCombinations(currentQuotation, EnrichedPricesChunk, minimumSpreadPercentage))
    combinations.push(...sellCombinations(currentQuotation, EnrichedPricesChunk, minimumSpreadPercentage))
  }
  return combinations
}


/**
 * Combinations where "currentQuotation" is buy side.
 * */
function buyCombinations(currentPrice: EnrichedPriceV2, prices: EnrichedPriceV2[], minimumSpreadPercentage: number): ICombination[] {
  const currPriceNetworks = currentPrice.networkBase.filter(network => network.withdraw).map(network => network.network)
  const otherPrices = prices.map(price => ({
      ...price,
      baseNetworkNames: price.networkBase.filter(network => network.deposit).map(network => network.network)
  }))


  const buyEligible = otherPrices.filter(price =>
    currentPrice.buyPrice < price.sellPrice &&
    // It checks if the spread is high enough to be profitable
    getSpreadPercentage(currentPrice.buyPrice, price.sellPrice) >= minimumSpreadPercentage &&
    // Check if networks corresponding --> We should test with substring because networks names aren't normalized (exemple: Sometime we have BTCERC20 & ERC20 that correspond to the same network )
    price.baseNetworkNames.some(network => currPriceNetworks.some(currNetwork => (currNetwork.includes(network) || network.includes(currNetwork) ) ) )
  )

  return buyEligible.map((price) : ICombination => {
    const {market: buyMarket, base, quote} = currentPrice.info
    const {market: sellMarket} = price.info

    return {
      name: getCombinationId(buyMarket, sellMarket, base, quote),
      efficiency: getSpreadPercentage(currentPrice.buyPrice, price.sellPrice),
      base,
      quote,
      pair: `${base}_${quote}`,
      volume: currentPrice.qty,
      buy : {
        market: buyMarket,
        symbol: `${buyMarket}_${base}_${quote}`,
        price: currentPrice.buyPrice,
        networksQuote: currentPrice.networkBase
      },
      sell : {
        market: sellMarket,
        symbol: `${sellMarket}_${base}_${quote}`,
        price: price.sellPrice,
        networksQuote: price.networkBase
      },
    }

  })
}


/**
 * Combinations where "currentQuotation" is sell side.
 * */
function sellCombinations(currentPrice: EnrichedPriceV2, prices: EnrichedPriceV2[], minimumSpreadPercentage: number): ICombination[] {
  const currPriceNetworks = currentPrice.networkBase.filter(network => network.deposit).map(network => network.network)
  const otherPrices = prices.map(price => ({
    ...price,
    baseNetworkNames: price.networkBase.filter(network => network.withdraw).map(network => network.network)

  }))

  const sellEligible = otherPrices.filter(price =>
    price.buyPrice < currentPrice.sellPrice &&
    // It checks if the spread is high enough to be profitable
    getSpreadPercentage(price.buyPrice, currentPrice.sellPrice) >= minimumSpreadPercentage &&
    // Check if networks corresponding --> We should test with substring because networks names aren't normalized (exemple: Sometime we have BTCERC20 & ERC20 that correspond to the same network )
    price.baseNetworkNames.some(network => currPriceNetworks.some(currNetwork => (currNetwork.includes(network) || network.includes(currNetwork) ) ) )
  )

  return sellEligible.map((price) : ICombination => {
    const {market: buyMarket, base, quote}  = price.info
    const {market: sellMarket} = currentPrice.info

    return {
      name: getCombinationId(buyMarket, sellMarket, base, quote),
      efficiency: getSpreadPercentage(price.buyPrice, currentPrice.sellPrice),
      base,
      quote,
      pair: `${base}_${quote}`,
      volume: currentPrice.qty,
      buy : {
        market: buyMarket,
        symbol: `${buyMarket}_${base}_${quote}`,
        price: price.buyPrice,
        networksQuote: price.networkBase
      },
      sell : {
        market: sellMarket,
        symbol: `${sellMarket}_${base}_${quote}`,
        price: currentPrice.sellPrice,
        networksQuote: currentPrice.networkBase
      },
    }

  })
}

function getCombinationId(buyMarket, sellMarket, base, quote) {
  return `${buyMarket}_${sellMarket}_${base}_${quote}`
}

function getSpreadPercentage(buyPrice: number, sellPrice: number): number {
  const mainNumber: number = buyPrice
  const subNumber: number = sellPrice - buyPrice
  const percentage = subNumber / mainNumber * 100
  return Math.round((percentage + Number.EPSILON) * 100) / 100
}


async function getSymbols () : Promise<LightSymbol[]> {

  const aggregateSymbols = [
    {$match: {"exclusion.isExclude" : false}},
    {$lookup: {
        from: "pairs",
        localField: "pair",
        foreignField: "name",
        as: "pair"
      }
    },
    {$lookup: {
        from: "markets",
        localField: "market",
        foreignField: "name",
        as: "market"
      }
    },
    {$unwind: "$pair"},
    {$unwind: "$market"},
    {$match: {"market.exclusion.isExclude" : false, "pair.exclusion.isExclude" : false}},
    {$project: { name: 1, market: "$market.name", base: 1, quote: 1, pair: "$pair.name", symbolCoinapi: 1 } }
  ]
  const symbols = await modelSymbol.aggregate<LightSymbol>(aggregateSymbols)
  console.log('symbolFOund', symbols.length)
  return symbols
}
