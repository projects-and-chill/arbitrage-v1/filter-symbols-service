import {connect} from 'mongoose'
import {NODE_ENV} from "./globals";

export function dbConnexion(){
  const {
    MONGO_PORT,
    MONGO_DB,
    MONGO_HOSTNAME,
    MONGO_USERNAME : user,
    MONGO_PASSWORD : pwd,
    MONGO_URI
  } = process.env

  const options = {
    useNewUrlParser : true,
    useUnifiedTopology : true,
    useCreateIndex : true ,
    useFindAndModify: false
  }

  let url : string
  if(MONGO_URI)
    url = `mongodb+srv://${user}:${pwd}@${MONGO_URI}/${MONGO_DB}?retryWrites=true&w=majority`
  else
    url = `mongodb://${user}:${pwd}@${MONGO_HOSTNAME}:${MONGO_PORT}/${MONGO_DB}`

  connect(url,options)
    .then(()=>{
    console.log('BDD MONGO EST CONNECTE SUR : ', url)
  })
    .catch((err)=>{
      console.log('IL Y A UNE UNE ERREUR DE BASE DE DONNE !! : ',`\n L'url DB : ${url}`,`\nl'erreur est : \n`,err)
    })
}
