import { model, Schema } from "mongoose"
import {IComparationConfig} from "../interfaces/IComparationConfig";

const schema = new Schema<IComparationConfig>({
  cctxPair: { type: String, required: true },
  minimumEfficiency: { type: Number, required: true },
  baseUsd: { type: Number, required: true },
  usdToSpend: { type: Number, required: true }
})

export const modelComparaisonConfig = model<IComparationConfig>("comparationConfigs", schema)
