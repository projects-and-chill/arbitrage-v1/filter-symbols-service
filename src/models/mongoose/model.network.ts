import {Document, model, Schema} from 'mongoose'
import {Network} from "../interfaces/network";

interface INetworkDoc extends Document, Network {}

const schema = new Schema({
    name: {type: String, required: true},
    network: {type: String, required: true},
    asset: {type: String, required: true},
    market: {type: String, required: true},
    active: {type: Boolean, required: true},
    deposit: {type: Boolean, required: true},
    withdraw: {type: Boolean, required: true},
    date : {type : Date, default : ()=> new Date() },
})

export const modelNetwork = model<INetworkDoc>('networks', schema)
