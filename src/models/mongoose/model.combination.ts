import {Document, model, Schema} from 'mongoose'
import {ICombination} from "../../services/calculBestsV2/build-combinations";

export interface IEnrichedCombination {
  combinationGroup: number
  usdToSpend: number
}

interface ICombinationMongo extends Document, ICombination, IEnrichedCombination {}

const schema = new Schema({
  name : {type : String, required: true},
  combinationGroup : {type : Number, required: true},
  usdToSpend : {type : Number, required: true},
  pair: {type : String, required: true},
  base: {type : String, required: true},
  quote: {type : String, required: true},
  efficiency: {type : Number, required: true},
  volume: {type : Number, required: true},
  buy: {
    symbol: {type : String, required: true},
    market: {type : String, required: true},
    networksQuote: [{type : Schema.Types.Mixed, required: true}],
    price: {type : Number, required: true},
  },
  sell: {
    symbol: {type : String, required: true},
    market: {type : String, required: true},
    networksQuote: [{type : Schema.Types.Mixed, required: true}],
    price: {type : Number, required: true},
  },
  date : {type : Date, default : ()=> new Date()},
});

export const modelCombination = model<ICombinationMongo>('combinations', schema)
