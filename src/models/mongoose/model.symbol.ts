import {Document, model, Schema} from 'mongoose'

import {Symbol} from "../interfaces/symbol";

interface ISymbolDoc extends Document,Symbol {}

const schema = new Schema({
    name : {type : String, required: true,},
    market : {type : String, required: true},
    pair : {type : String, required: true},
    symbolCoinapi : {type : String, required: true},
    base : {type : String, required: true},
    quote : {type : String, required: true},
    exclusion : {
        isExclude : {type : Boolean, required: true},
        reasons : [{type : String, required: true}],
        severity : {type : Number, required: true},
        excludeBy : {type : String, required: true},
        note : {type : String, required: false},
        date : {type : String, required: false}
    },
    networks: [{
        network: {type: String, required: true},
        active: {type: Boolean, required: false},
        deposit: {type: Boolean, required: false},
        withdraw: {type: Boolean, required: false},
    }],
    isfor : {type : Object, required : true},
    date : {type : Date, default : ()=> new Date() },
})

let modelSymbol = model<ISymbolDoc>('symbols', schema)
export default modelSymbol
