import { model, Schema } from "mongoose"

export interface ICredential {
  market: string,
  apiKey?: string,
  secret?: string,
  password?: string,
}

const schema = new Schema<ICredential>({
  market: { type: String, required: true },
  apiKey: { type: String, required: false },
  secret: { type: String, required: false },
  password: { type: String, required: false }
})

export const modelCredential = model<ICredential>("credentials", schema)
