import {Document, model, Schema} from 'mongoose'
import {Reason} from "../interfaces/reason";

interface IReasonDoc extends Document,Reason {}

const schema = new Schema({
  status : {type : String, required:"Vous devez entrer le status", },
  for : {type : String, required:"Vous devez entrer quel type d'item est exclus"},
  description : {type : String, required:"Vous devez entrer la description"},
});


let modelReason = model<IReasonDoc>('reasons', schema)

export default modelReason
