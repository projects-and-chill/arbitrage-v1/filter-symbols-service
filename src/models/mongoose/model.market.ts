import {model, Schema, Document} from 'mongoose'
import {Market} from "../interfaces/market";


interface IMarketDoc extends Document,Market {}

const schema = new Schema({
  name : {type : String, required:"Vous devez renseigner l'id de l'Exchange", },
  longName : {type : String, required: true },
  pairsCount : {type : Number, required: true },
  website: {type : String, required: true },
  pairsForThis :{type : Number, default : 0 },
  exclusion : {
    isExclude : {type : Boolean, default : false},
    reasons : [{type : String, required : 'Vous devez renseigner aumoins 1 raison'}],
    severity : {type: Number, required : 'vous devez entrer la severité'},
    excludeBy : {type: String, required : 'vous devez entrer le nom du commenditaire'},
    note : {type: String, default : ''},
    date : {type : Date}
  },
  date : {type : Date, default : ()=> new Date() },
})

let modelMarket = model<IMarketDoc>('markets', schema)
export default modelMarket
