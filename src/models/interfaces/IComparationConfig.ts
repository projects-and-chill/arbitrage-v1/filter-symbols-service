export interface IComparationConfig {
  cctxPair: string,
  baseUsd: number,
  minimumEfficiency: number,
  usdToSpend: number,
}
