export interface CurrencyNetwork {
  id: string,
  info?: Record<string, any>,
  network: string,
  limits?: Record<string, any>,
  active?: boolean,
  deposit: boolean,
  depositEnable?: boolean,
  withdraw?: boolean,
  withdrawEnable?: boolean,
  fee?: number,
  precision?: number
}