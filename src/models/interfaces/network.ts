export interface Network {
  name: string,
  network: string,
  asset: string,
  market: string,
  active: boolean | null,
  deposit: boolean | null,
  withdraw: boolean | null,
}