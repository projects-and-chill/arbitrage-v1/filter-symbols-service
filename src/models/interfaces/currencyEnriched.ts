import {Currency} from "ccxt";
import {CurrencyNetwork} from "./currencyNetwork";

export interface CurrencyEnriched extends Currency {
  networks?: Record<string, CurrencyNetwork>
}